import {TestController} from "./controller/test.controller";

import express from "express";
import {connect} from "mongoose";

export function createApp() {
    const testController = new TestController();
    const env = process.env.NODE_ENV;
    const dbConnection = env === "test"
        ? "mongodb://treeline:treeline@localhost:27017/test"
        : "mongodb://treeline:treeline@localhost:27017/jshiring";

    connect(dbConnection, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    const app = express();
    app.use(express.json());
    app.use("/tests", testController.getRouter());
    return app;
}

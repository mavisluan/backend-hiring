import mongoose from "mongoose";

export interface ITest extends mongoose.Document {
    firstName: string;
    lastName: string;
    age: number;
    country: string;
    positive: boolean;
}

export const TestSchema = new mongoose.Schema({
    firstName: {
        type: String,
        minlength: [2, 'Minimum name length is 2'],
    },
    lastName: {
        type: String,
        minlength: [2, 'Minimum name length is 2'],
    },
    age: Number,
    country: {
      type: String,
      minlength: [2, 'Minimum country code length is 2'],
    },
    positive: Boolean,
}, {timestamps: true});

const Test = mongoose.model<ITest>("Test", TestSchema);
export default Test;

const nodeGeocoder = require("node-geocoder");

const options = {
    provider: 'openstreetmap'
};

const geoCoder = nodeGeocoder(options);

const geoToCountry = async ({lat, lon}) => {
    try {
        const res = await geoCoder.reverse({lat, lon});
        return res[0].countryCode;
    } catch (error) {
        console.log("err", error);
        throw {message: "Unable to locate the country with the input values"};
    }
}

module.exports = geoToCountry;

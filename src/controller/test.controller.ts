import {Router} from "express";
import getTests from "../service/getTests";
import createTest from "../service/createTest";
import {check} from "express-validator";

export class TestController {
    getRouter(): Router {
        const router = Router();
        // CREATE a test record
        router.post("/", [
            check('firstName')
                .isLength({ min: 2 })
                .withMessage('Name must be at least 2 characters'),
            check('lastName')
                .isLength({ min: 2 })
                .withMessage('Name must be at least 2 characters'),
            check('age')
                .isNumeric()
                .withMessage('Age must be a number'),
            check('positive')
                .isBoolean()
                .withMessage('Positive must be a Boolean'),
            check('lat')
                .isNumeric()
                .withMessage('lat must be a number'),
            check('lon')
                .isNumeric()
                .withMessage('lon must be a number'),
        ], createTest);

        // GET total tests and total positive tests by day or total tests per country
        router.get("/per/:groupBy", getTests);
        return router;
    }
}

import {Request, Response} from "express";
import Test from "../model/test";
import { validationResult} from "express-validator";

const geoToCountry = require("../helper/geoToCountry");

const createTest = async (req: Request, res: Response) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        const errorsArray = errors.array();
        const errorMsgs = errorsArray.map(err => {
            return err.msg;
        })
        return res.status(422).json({errors: errorMsgs});
    }

    try {
        const {firstName, lastName, age, lat, lon, positive = false} = req.body;
        const country = await geoToCountry({lat, lon});

        const test = new Test({
            firstName,
            lastName,
            age,
            country,
            positive
        });

        const savedTest = await test.save();
        return res.status(200).send({message: "Created a test successfully", test: savedTest});
    } catch (error) {
        return res.status(400).send(error.message);
    }
}

export default createTest;

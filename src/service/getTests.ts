import {Request, Response} from "express";
import Test from "../model/test";

const getTests = async (req: Request, res: Response) => {
    const {groupBy} = req.params;
    // validation
    if (groupBy !== "day" && groupBy !== "country") {
        return res.status(400).send({message: "Please choose to group by day or country only"})
    }

    let aggregation;
    try {
        if (groupBy === "day") {
            aggregation = [
                {
                    $project: {
                        yearMonthDayUTC: {$dateToString: {format: "%Y-%m-%d", date: "$createdAt"}},
                        _id: 0,
                    }
                },
                {
                    $group: {
                        _id: "$yearMonthDayUTC",
                        totalTests: {$sum: 1}
                    }
                },
                {
                    $project: {
                        testDate: "$_id",
                        _id: 0,
                        totalTests: 1,
                    }
                },
                {
                    $sort: {testDate: 1}
                }
            ];
            const totalTestsByDay = await Test.aggregate(aggregation);
            const positiveTestsByDay = await Test.aggregate([
                {
                    $match: {positive: true}
                },
                ...aggregation
            ]);

            return res.status(200).send({totalTestsByDay, positiveTestsByDay});
        } else if (groupBy === "country") {
            aggregation = [
                {
                    $group: {
                        _id: "$country",
                        totalTests: {$sum: 1}
                    }
                },
                {
                    $project: {
                        _id: 0,
                        country: "$_id",
                        totalTests: 1
                    }
                }
            ];

            const totalTestsByCountry = await Test.aggregate(aggregation);
            return res.status(200).send({totalTestsByCountry});
        }
    } catch (error) {
        return res.status(400).send(error);
    }
}

export default getTests;

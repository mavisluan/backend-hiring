const tests = [
    {
        "firstName": "Helen",
        "lastName": "Molly",
        "age": 55,
        "country": "FR",
        "positive": true,
        "createdAt": "2021-07-25T03:40:04.936Z",
        "updatedAt": "2021-07-25T03:40:04.936Z",
    },
    {
        "firstName": "Jerry",
        "lastName": "Jackson",
        "age": 25,
        "country": "US",
        "positive": true,
        "createdAt": "2021-07-21T03:40:04.936Z",
        "updatedAt": "2021-07-21T03:40:04.936Z",
    },
    {
        "firstName": "Alison",
        "lastName": "Wang",
        "age": 18,
        "country": "UK",
        "positive": false,
        "createdAt": "2021-07-23T03:40:04.936Z",
        "updatedAt": "2021-07-23T03:40:04.936Z",
    },
    {
        "firstName": "Katie",
        "lastName": "Williams",
        "age": 23,
        "country": "UK",
        "positive": true,
        "createdAt": "2021-07-20T03:40:04.936Z",
        "updatedAt": "2021-07-20T03:40:04.936Z",
    },
    {
        "firstName": "Jason",
        "lastName": "Davis",
        "age": 13,
        "country": "UK",
        "positive": false,
        "createdAt": "2021-07-20T03:40:04.936Z",
        "updatedAt": "2021-07-20T03:40:04.936Z",
    },
]

export default tests;

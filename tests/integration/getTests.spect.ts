import request from "supertest";
import {createApp} from "../../src/app";

describe("GET tests", () => {
    it("returns 400 on GET /tests/per/{not day or country}", async () => {
        try {
            const res = await request(createApp()).get("/tests/per/random");
            expect(400);
            expect(res.body).toHaveProperty("message");
            expect(res.body.message).toBe("Please choose to group by day or country only");
        } catch (err) {
            console.log("err", err)
        }
    });

    it("returns 200 and totalTestsByDay, positiveTestsByDay on GET /tests/per/day", async () => {
        try {
            const res = await request(createApp()).get("/tests/per/day");
            expect(200);
            expect(res.body).toHaveProperty("totalTestsByDay");
            expect(res.body).toHaveProperty("positiveTestsByDay");
        } catch (err) {
            console.log("err", err)
        }
    });

    it("returns 200 and totalTestsByCountry on GET /tests/per/country", async () => {
        try {
            const res = await request(createApp()).get("/tests/per/country");
            console.log("TESTS", res.body)
            expect(200);
            expect(res.body).toHaveProperty("totalTestsByCountry");
        } catch (err) {
            console.log("err", err)
        }
    });
});

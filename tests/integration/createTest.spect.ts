import request from "supertest";
import {createApp} from "../../src/app";
import Test from "../../src/model/test";
import tests from "../seed";
// clear the test db collection records before create one
beforeEach(async() => {
    return new Promise((resolve, reject) => {
        Test.deleteMany({}).then(() => {
            Test.insertMany(tests);
        });
        resolve();
    });
})

describe("CREATE tests /tests", () => {
    it("returns 422 if any required property is missing ", async () => {
        try {
            const data = {
                firstName: "Jenny",
                lastName: "Smith",
                age: 18,
                lat: 38.66,
                lon: -78.43
            };

            const res = await request(createApp())
                .post("/tests")
                .send(data)
            expect(422);
            expect(res.body).toHaveProperty("errors");
            expect(res.body.errors).toHaveLength(1);
            expect(res.body.errors).toEqual(
                expect.arrayContaining(['Positive must be a Boolean']),
            );
        } catch (err) {
            console.log("22222err", err)
        }
    });

    it("returns 200 and created test if all required properties exist", async () => {
        try {
            const data = {
                firstName: "Alice",
                lastName: "Johnson",
                age: 18,
                lat: 38.66,
                lon: -78.43,
                positive: false
            };

            const res = await request(createApp())
                .post("/tests")
                .send(data)
            expect(200);
            expect(res.body).toHaveProperty("message");
            expect(res.body.message).toBe("Created a test successfully");
            expect(res.body).toHaveProperty("test");
            expect(res.body.test).toMatchObject({
                firstName: "Alice",
                lastName: "Johnson",
                positive: false
            });
        } catch (err) {
            console.log("err", err)
        }
    });
});
